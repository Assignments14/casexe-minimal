<?php


namespace Api\Controllers;


class PrizeController
{
    public function random(): array
    {
        return [
            'id' => 1,
            'name' => 'Thing1',
            'type' => 3,
            'value' => 1,
            'convertible' => true,
            'converted_value' => 100,
        ];
    }
}