<?php


namespace Api\Controllers;


class AuthController
{
    public function token(): array
    {
       return [
           'access_token' => API_CLIENT_TOKEN,
       ];
    }
}