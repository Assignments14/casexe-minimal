<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/config.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$path = $_SERVER['PATH_INFO'];
[, $controller, $action] = explode('/', ltrim($path));

function dispatch_route(string $controller, string $action): string
{
    $controller = \ucfirst($controller);
    $full_controller_class = 'Api\\Controllers\\' . $controller . 'Controller';
    $dispatcher = new $full_controller_class;
    return json_encode($dispatcher->$action());
}

echo dispatch_route($controller, $action);
