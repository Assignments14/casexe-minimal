class PrizeTransformer {
    static fetch(prize) {
        return {
            id: prize.id,
            type: prize.type,
            name: prize.name,
            value: prize.value,
            convertible: prize.convertible,
            convertedValue: prize.converted_value,
        };
    }
}

export default PrizeTransformer;
