
export default [
    {
        path: '/prize/random',
        name: 'prize.random',
        component: resolve => require(['@/pages/prize/random.vue'], resolve),
        meta: {
            auth: true,
        },
    },
    {
        path: '/prize/show',
        name: 'prize.show',
        component: resolve => require(['@/pages/prize/show.vue'], resolve),
        meta: {
            auth: true,
        },
    },
    {
        path: '/user/login',
        name: 'auth.login',
        component: resolve => require(['@/pages/auth/login.vue'], resolve),
        meta: {
            guest: true,
        },
    },
    {
        path: '/404',
        name: 'not-found',
        component: resolve => require(['@/pages/not-found.vue'], resolve),
    },
    {
        path: '/',
        redirect: '/prize/random',
    },
    {
        path: '/*',
        redirect: '/404',
    },
];
