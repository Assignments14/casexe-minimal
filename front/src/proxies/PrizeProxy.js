import BaseProxy from './BaseProxy';

class PrizeProxy extends BaseProxy {
    constructor() {
        super('prize');
    }

    random() {
        return this.submit('get', `/${this.endpoint}/random`);
    }
}

export default PrizeProxy;
