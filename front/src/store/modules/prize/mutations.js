// import Vue from 'vue';

import {
    RANDOM,
} from './mutation-types';

export default {
    [RANDOM](state, { item }) {
        state.item = item;
    },
};
