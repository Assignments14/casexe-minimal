import Vue from 'vue';
import * as types from './mutation-types';
import PrizeProxy from '@/proxies/PrizeProxy';
import PrizeTransformer from '@/transformers/PrizeTransformer';

const proxy = new PrizeProxy();

export const random = ({ commit }) => {
    proxy.random().then((response) => {
        const prize = {
            item: PrizeTransformer.fetch(response),
        };
        commit(types.RANDOM, prize);
        Vue.router.push({
            name: 'prize.show',
        });
    });
};

export default {
    random,
};
