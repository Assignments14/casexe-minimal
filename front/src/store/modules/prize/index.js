import state from './state';
import mutations from './mutations';
import actions from './actions';

export default {
    namespaced: true, // auth/action
    state,
    actions,
    mutations,
};
