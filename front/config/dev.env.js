'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    API_LOCATION: '"http://localhost:8081/index.php"',
    API_CLIENT_ID: '"4"',
    API_CLIENT_SECRET: '"xDzE+hY1ZfVpwzyDm3ZvXF3x2QRaKr05ahQRFNLSbY8="',
})
